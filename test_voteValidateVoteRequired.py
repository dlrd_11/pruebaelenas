import time
import unittest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


class VoteOneItem(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        driver = self.driver
        driver.get("http://127.0.0.1:8000/polls/1/")
        driver.maximize_window()
        driver.implicitly_wait(30)
    
    def test_vote_again_required(self):
        driver = self.driver
        
        button_vote = driver.find_element_by_xpath("//input[@type='submit']")
        self.assertTrue(button_vote)
        button_vote.click()
        
        text_informative = driver.find_element_by_xpath("/html/body/p/strong")
        self.assertTrue(text_informative)
        
        time.sleep(2)
    
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main(verbosity = 2)
    