import time
import unittest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


class VoteOneItem(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        driver = self.driver
        driver.get("http://127.0.0.1:8000/polls/1/")
        driver.maximize_window()
        driver.implicitly_wait(30)
    
    def test_vote_again(self):
        driver = self.driver
        radioButton_the_sky = driver.find_element_by_id("choice2")
        self.assertTrue(radioButton_the_sky)
        radioButton_the_sky.click()
        
        text_the_sky = driver.find_element_by_xpath("/html/body/form/label[2]")
        driver.implicitly_wait(1)
        self.assertTrue(text_the_sky)
        self.assertEqual("The sky", text_the_sky.text)
        
        button_vote = driver.find_element_by_xpath("//input[@type='submit']")
        self.assertTrue(button_vote)
        button_vote.click()
        
        link_vote_again = driver.find_element_by_xpath("//*[contains(text(), 'Vote again?')]")
        link_vote_again.click()

        tittle = driver.find_element_by_xpath("/html/body/h1")
        driver.implicitly_wait(1)
        self.assertTrue(tittle)
        
        time.sleep(2)
    
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main(verbosity = 2)
    