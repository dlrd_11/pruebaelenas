import unittest
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver


class VoteOneItem(unittest.TestCase):


    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        driver = self.driver
        driver.get("http://127.0.0.1:8000/polls/1/")
        driver.maximize_window()
        driver.implicitly_wait(30)
    
    def test_visibility(self):
        driver = self.driver
        tittle = driver.find_element_by_xpath("/html/body/h1")
        driver.implicitly_wait(1)
        self.assertTrue(tittle)

    def test_select_no_much(self):
        driver = self.driver
        radioButton_no_much = driver.find_element_by_id("choice1")
        self.assertTrue(radioButton_no_much)
        radioButton_no_much.click()
        text_no_much = driver.find_element_by_xpath("/html/body/form/label[1]")
        driver.implicitly_wait(1)
        self.assertTrue(text_no_much)
        self.assertEqual("Not much", text_no_much.text)
    
    def test_select_button_vote(self):
        driver = self.driver
        button_vote = driver.find_element_by_xpath("//input[@type='submit']")
        self.assertTrue(button_vote)
        button_vote.click()
    
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main(verbosity = 2)
    